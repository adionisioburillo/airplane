//
//  HomeInteractor.swift
//  Airplane
//
//  Created by Alejandro Dionisio on 03/09/2020.
//  Copyright © 2020 adionisio. All rights reserved.
//

import Foundation

protocol HomeUseCase {
    
}

class HomeInteractor {
    
}

extension HomeInteractor: HomeUseCase{
    
}
