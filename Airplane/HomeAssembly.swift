//
//  HomeAssembly.swift
//  Airplane
//
//  Created by Alejandro Dionisio on 03/09/2020.
//  Copyright © 2020 adionisio. All rights reserved.
//

import UIKit

class HomeAssembly {
    
    static func build() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let view = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let interactor = HomeInteractor()
        let router = HomeRouter(view: view)
        let presenter = HomePresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        view.view.backgroundColor = .orange
        return view
    }
    
}
