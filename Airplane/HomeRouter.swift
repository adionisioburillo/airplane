//
//  HomeRouter.swift
//  Airplane
//
//  Created by Alejandro Dionisio on 03/09/2020.
//  Copyright © 2020 adionisio. All rights reserved.
//

import UIKit


protocol HomeRouting {
    
}

class HomeRouter {
    
    var view: UIViewController
    
    init(view: UIViewController) {
        self.view = view
    }
}

extension HomeRouter: HomeRouting {
    
}
